<?php

namespace Drupal\Tests\marketo_ma\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\marketo_ma\Service\MarketoMaApiClient;
use Drupal\marketo_ma\Service\MarketoMaApiClientInterface;
use Drupal\marketo_ma\Service\MarketoMaServiceInterface;
use NecLimDul\MarketoRest\Hacks\API\LeadsApi;
use NecLimDul\MarketoRest\Hacks\API\ActivitiesApi;
use Psr\Log\LoggerInterface;

/**
 * Base for Marketo MA kernel tests.
 *
 * @group marketo_ma
 */
abstract class MarketoMaKernelTestBase extends KernelTestBase {

  /**
   * The marketo_ma service.
   *
   * @var \Drupal\marketo_ma\Service\MarketoMaServiceInterface
   */
  protected $service;

  /**
   * The marketo_ma rest client service.
   *
   * @var \Drupal\marketo_ma\Service\MarketoMaApiClientInterface
   */
  protected $apiClient;

  /**
   * The marketo settings config.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'user',
    'marketo_ma',
  ];

  /**
   * The "real" client that would talk to marketo.
   *
   * @var \CSD\Marketo\Client|\Prophecy\Prophecy\ObjectProphecy
   */
  protected $realClient;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Install config for this module.
    $this->installConfig('marketo_ma');
    // Install module schema.
    $this->installSchema('marketo_ma', 'marketo_ma_lead_fields');

    $config_factory = \Drupal::configFactory();
    // Get the API client service.
    $this->realClient = $this->prophesize(MarketoMaApiClientInterface::class);
    $this->apiClient = new class (\Drupal::logger('marketo_ma'), $this->realClient->reveal()) extends MarketoMaApiClient {

      /**
       * Construct our mock service.
       */
      public function __construct(
        LoggerInterface $logger,
        MarketoMaApiClientInterface $apiClient
      ) {
        $lead_api = new LeadsApi();
        $activity_api = new ActivitiesApi();
        parent::__construct($lead_api, $activity_api, $logger);
      }

    };

    // Get the marketo_ma service.
    $this->service = \Drupal::service('marketo_ma');

    $this->config = $config_factory->getEditable(MarketoMaServiceInterface::MARKETO_MA_CONFIG_NAME);
  }

}
