<?php

namespace Drupal\Tests\marketo_ma\Traits;

use Drupal\marketo_ma\FieldDefinitionSet;
use Drupal\marketo_ma\Service\MarketoMaServiceInterface;

/**
 * Traits to help setup fields on a site for testing marketo.
 */
trait TestFieldsTrait {

  /**
   * Mock sample field data in database and config.
   */
  public function createSampleFields() {
    $t = new FieldDefinitionSet();
    $t->add([
      'id' => 1,
      'displayName' => 'Name',
      'dataType' => 'string',
      'rest' => ['name' => 'firstName', 'readOnly' => FALSE],
      'soap' => ['name' => 'first_name', 'readOnly' => FALSE],
    ]);
    $t->add([
      'id' => 3,
      'displayName' => 'Email',
      'dataType' => 'string',
      'rest' => ['name' => 'email', 'readOnly' => FALSE],
      'soap' => ['name' => 'Email', 'readOnly' => FALSE],
    ]);

    \Drupal::configFactory()->getEditable(MarketoMaServiceInterface::MARKETO_MA_CONFIG_NAME)
      ->set('field.enabled_fields', ['firstName', 'email'])
      ->save();
  }

}
