<?php

namespace Drupal\Tests\marketo_ma\Traits;

use Drupal\marketo_ma\Service\MarketoMaServiceInterface;

/**
 * Trait with tools for setting up and testing a marketo sites.
 */
trait MarketoMaFunctionalHelperTrait {

  /**
   * The munchkin settings config object.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * The marketo account id for the site.
   *
   * @var string
   */
  protected $accountId;

  /**
   * The munchkin private key.
   *
   * @var string
   */
  protected $privateKey;

  /**
   * The rest client id for the site.
   *
   * @var string
   */
  protected $restClientId;

  /**
   * The rest client secret for the site.
   *
   * @var string
   */
  protected $restClientSecret;

  /**
   * Mock general config for a functional site.
   */
  protected function mockConfig() {
    // @todo Randomize?
    $this->accountId = 'asdf';
    $this->privateKey = 'asdf';
    $this->restClientId = 'asdf';
    $this->restClientSecret = 'asdf';

    // Set up required settings.
    $this->config = \Drupal::configFactory()->getEditable(MarketoMaServiceInterface::MARKETO_MA_CONFIG_NAME);
    $this->config
      ->set('instance_host', 'https://example.com/')
      ->set('munchkin.account_id', $this->accountId)
      ->set('munchkin.api_private_key', $this->privateKey)
      ->set('rest.client_id', $this->restClientId)
      ->set('rest.client_secret', $this->restClientSecret)
      ->save();
  }

  /**
   * Gets drupal settings and parses into an php array.
   *
   * @return array
   *   The drupal settings object.
   */
  protected function getDrupalSettings() {
    // Get the active drupal settings from the session.
    return $this->getSession()->evaluateScript('function(){ if (typeof drupalSettings !== \'undefined\') { return drupalSettings; }}()');
  }

  /**
   * Assert munchkin tracking is present on the page.
   *
   * @param bool $tracking
   *   Expected value of the tracking flag.
   * @param array|null $actions
   *   List of expected actions.
   */
  protected function assertMunchkinTracking($tracking = TRUE, $actions = NULL) {
    $drupal_settings = $this->getDrupalSettings();
    $url = $this->getSession()->getCurrentUrl();
    $this->assertTrue(!empty($drupal_settings['marketo_ma']), 'Marketo settings included: ' . $url);
    $marketo_ma_settings = $drupal_settings['marketo_ma'];
    $this->assertEquals($this->accountId, $marketo_ma_settings['key'], 'Munchkin code included: ' . $url);
    $this->assertEquals($tracking, $marketo_ma_settings['track'], 'Correct marketo track flag status: ' . $url);
    if (isset($actions)) {
      $this->assertFalse(empty($marketo_ma_settings['actions']), 'Actions: ' . $url);
      foreach ($actions as $k => $v) {
        $this->assertTrue(!empty($marketo_ma_settings['actions'][0]['hash']), 'Action hash exists');
        // These are a bit dynamic and harder to assert.
        unset($marketo_ma_settings['actions'][$k]['hash']);
        unset($marketo_ma_settings['actions'][$k]['data']['cookies']);
      }
      $this->assertEquals($actions, $marketo_ma_settings['actions']);
    }
    else {
      $this->assertTrue(empty($marketo_ma_settings['actions']), 'No actions: ' . $url);
    }
  }

  /**
   * Assert that no munchkin tracking is present on a page.
   */
  protected function assertNoMunchkinTracking() {
    $drupal_settings = $this->getDrupalSettings();
    $url = $this->getSession()->getCurrentUrl();
    $this->assertTrue(empty($drupal_settings['marketo_ma']), 'No tracking on page: ' . $url);
  }

}
