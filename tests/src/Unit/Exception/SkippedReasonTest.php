<?php

namespace Drupal\Tests\marketo_ma\Unit\Exception;

use Drupal\marketo_ma\Exception\SkippedReason;
use Drupal\Tests\UnitTestCase;

/**
 * @coversDefaultClass \Drupal\marketo_ma\Exception\SkippedReason
 */
class SkippedReasonTest extends UnitTestCase {

  /**
   * @covers ::__construct
   * @covers ::getCode
   */
  public function testGetCode() {
    $this->assertSame('123', (new SkippedReason('123', 'foo'))->getCode());
  }

  /**
   * @covers ::__construct
   * @covers ::getReason
   */
  public function testGetReason() {
    $this->assertSame('foo', (new SkippedReason('123', 'foo'))->getReason());
  }

  /**
   * @covers ::jsonSerialize
   */
  public function testSerialize() {
    $reason = new SkippedReason('123', 'foo');
    $this->assertSame('{"code":"123","reason":"foo"}', json_encode($reason));
  }

}
