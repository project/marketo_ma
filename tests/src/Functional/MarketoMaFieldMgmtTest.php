<?php

namespace Drupal\Tests\marketo_ma\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\marketo_ma\Traits\TestFieldsTrait;

/**
 * Marketo field management admin form tests.
 *
 * @group marketo_ma
 */
class MarketoMaFieldMgmtTest extends BrowserTestBase {

  use TestFieldsTrait;

  /**
   * {@inheritDoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritDoc}
   */
  protected static $modules = [
    'marketo_ma',
  ];

  /**
   * {@inheritDoc}
   */
  public function setUp(): void {
    parent::setUp();
    $this->createSampleFields();
  }

  /**
   * Test field management.
   */
  public function testFieldManagement() {
    $account = $this->createUser(['administer marketo']);
    $this->drupalLogin($account);
    $this->drupalGet('admin/config/services/marketo-ma/fields');
    $session = $this->assertSession();
    $session->checkboxChecked('edit-field-enabled-fields-email');
    $session->checkboxChecked('edit-field-enabled-fields-firstname');
    $this->submitForm([
      'field_enabled_fields[email]' => '',
      'field_enabled_fields[firstName]' => 'firstName',
    ], 'Save configuration');
    $session->checkboxNotChecked('edit-field-enabled-fields-email');
    $session->checkboxChecked('edit-field-enabled-fields-firstname');

  }

}
