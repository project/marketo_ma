<?xml version="1.0" encoding="UTF-8"?>
<ruleset name="Module">
  <file>.</file>

  <!-- txt,md -->
  <arg name="extensions" value="php,module,inc,install,test,profile,theme,css,info,yml"/>

  <!-- Initially include all Drupal and DrupalPractice sniffs. -->
  <rule ref="vendor/drupal/coder/coder_sniffer/Drupal"/>
  <rule ref="vendor/drupal/coder/coder_sniffer/DrupalPractice"/>
  <rule ref="DrupalPractice.Objects">
    <exclude name="DrupalPractice.Objects.GlobalDrupal.GlobalDrupal"/>
  </rule>
  <rule ref="Drupal.Commenting.VariableComment">
    <!-- PHPStan will handle this better. -->
    <exclude name="Drupal.Commenting.VariableComment.IncorrectVarType"/>
  </rule>
  <rule ref="Drupal.Commenting.FunctionComment">
    <!-- PHPStan will handle this better. -->
    <exclude name="Drupal.Commenting.FunctionComment.ParamTypeSpaces"/>
    <exclude name="Drupal.Commenting.FunctionComment.ReturnTypeSpaces"/>
  </rule>
  <rule ref="Drupal.Commenting.InlineComment">
    <!-- This isn't really enforced and is required in some cases. -->
    <exclude name="Drupal.Commenting.InlineComment.DocBlock"/>
  </rule>
  <rule ref="Drupal.Arrays.Array">
    <!-- Too many false positives -->
    <exclude name="Drupal.Arrays.Array.ArrayIndentation"/>
  </rule>
  <rule ref="Generic.CodeAnalysis.UselessOverridingMethod">
    <!-- PHPStan will handle this better. -->
    <exclude name="Generic.CodeAnalysis.UselessOverridingMethod.Found"/>
  </rule>

  <!-- Use 's' to print the full sniff name in the report. -->
  <!-- A '-' is prefixed to each of these, so s becomes -s, etc. -->
  <arg value="s"/>
  <arg value="-colors"/>
  <arg name='report-width' value='120'/>

  <!-- Ignore all files that match these patterns. They are matched against -->
  <!-- the full file path and there is an implied wildcard at each end. -->
  <!-- Periods must be escaped using \. -->
  <!-- The first two are key to isolating just this project's files. -->
  <exclude-pattern>web/</exclude-pattern>
  <exclude-pattern>vendor/</exclude-pattern>
  <exclude-pattern>\.patch</exclude-pattern>
  <exclude-pattern>interdiff</exclude-pattern>

</ruleset>
