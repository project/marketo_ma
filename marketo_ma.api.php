<?php

/**
 * @file
 * Hooks provided by Marketo MA.
 *
 * @todo Implement these alter hooks.
 */

use Drupal\marketo_ma\Lead;

/**
 * @addtogroup hooks
 * @{
 */

/**
 * This hook is executed when a lead is added to the queue for submission.
 *
 * @param \Drupal\marketo_ma\Lead $lead
 *   A lead object about to be submitted.
 *
 * @see marketo_ma_add_lead()
 */
function hook_marketo_ma_lead_alter(Lead $lead) {
  // Set or update the lead source for this lead.
  $lead->set('LeadSource', 'Foo');
}

/**
 * This hook is executed for a specific FIELDNAME when a lead is updated.
 *
 * FIELDNAME equates to valid Marketo field names such as:
 * - FirstName
 * - LastName.
 *
 * @param mixed $data
 *   The value of FIELDNAME.
 *
 * @see marketo_ma_add_lead()
 */
function hook_marketo_ma_lead_FIELDNAME_alter(&$data) {
  // Convert this specific field value to lowercase.
  $data = strtolower($data);
}

/**
 * @} End of "addtogroup hooks".
 */
