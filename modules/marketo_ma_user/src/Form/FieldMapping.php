<?php

namespace Drupal\marketo_ma_user\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\marketo_ma\MarketoFieldDefinition;
use Drupal\marketo_ma\Service\MarketoMaServiceInterface;
use Drupal\marketo_ma_user\Service\MarketoMaUserServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Marketo user field mapping configuration form.
 */
class FieldMapping extends ConfigFormBase {

  /**
   * The Marketo MA API client.
   *
   * @var \Drupal\marketo_ma\Service\MarketoMaServiceInterface
   */
  protected $marketoMaService;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * Constructs a \Drupal\marketo_ma\Form\MarketoMaSettings object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\marketo_ma\Service\MarketoMaServiceInterface $marketo_ma_service
   *   The Marketo MA API client.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    MarketoMaServiceInterface $marketo_ma_service,
    EntityFieldManagerInterface $entity_field_manager
  ) {
    parent::__construct($config_factory);
    $this->marketoMaService = $marketo_ma_service;
    $this->entityFieldManager = $entity_field_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('marketo_ma'),
      $container->get('entity_field.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [MarketoMaUserServiceInterface::MARKETO_MA_USER_CONFIG_NAME];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'marketo_ma_user_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /** @var array $form */
    $form = parent::buildForm($form, $form_state);

    // Get the configuration.
    $config = $this->config(MarketoMaUserServiceInterface::MARKETO_MA_USER_CONFIG_NAME);

    $form['help'] = [
      '#type' => 'markup',
      '#markup' => '<p>' . $this->t('If you do not see a required field, make sure its enabled in the main field management list.') . '</p>',
    ];
    $form['mapping'] = [
      '#type' => 'table',
      '#header' => [
        'title' => $this->t('User Field'),
        'mapping' => $this->t('Marketo Field'),
      ],
      '#empty' => $this->t('There are no user fields available for mapping.'),
    ];

    // Get mappings from config.
    $mapping = $config->get('mapping');
    // Get enabled marketo fields.
    $marketo_field_options = ['' => $this->t('None')] + $this->getMarketoFields();
    // Add an mapping select field for each user field.
    foreach ($this->getUserFields() as $field_name => $label) {
      $form['mapping'][$field_name] = [
        'title' => ['#markup' => $label],
        'mapping' => [
          '#type' => 'select',
          '#title' => $this->t('Select mapped component'),
          '#title_display' => 'hidden',
          '#options' => $marketo_field_options,
          '#default_value' => $mapping[$field_name] ?? FALSE,
        ],
      ];
    }

    // Add the validation and submit callbacks. Why??
    $form['#validate'][] = [$this, 'validateForm'];
    $form['#submit'][] = [$this, 'submitForm'];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    // Get the mapping values.
    $mapping = array_map(function ($form_value) {
      return $form_value['mapping'];
    }, $form_state->getValue('mapping'));

    $this->config(MarketoMaUserServiceInterface::MARKETO_MA_USER_CONFIG_NAME)
      ->set('mapping', array_filter($mapping))
      ->save();
  }

  /**
   * Returns the available marketo field.
   *
   * @return string[]
   *   Marketo field labels keyed by machine name.
   */
  protected function getMarketoFields() {
    return array_map(function (MarketoFieldDefinition $field) {
      return $field->getDisplayName();
    }, $this->marketoMaService->getEnabledFields());
  }

  /**
   * Returns all available user form field.
   *
   * @return string[]
   *   Additional field labels keyed by machine name.
   */
  protected function getUserFields() {
    $fields = $this->entityFieldManager->getFieldDefinitions('user', 'user');
    return array_map(function (FieldDefinitionInterface $field_definition) {
      return $field_definition->getLabel();
    }, $fields);
  }

}
