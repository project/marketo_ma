<?php

namespace Drupal\marketo_ma_user\Service;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Session\AccountProxy;
use Drupal\Core\State\StateInterface;
use Drupal\marketo_ma\Lead;
use Drupal\marketo_ma\Service\MarketoMaApiClientInterface;
use Drupal\marketo_ma\Service\MarketoMaServiceInterface;
use Drupal\user\UserInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Marketo User integration service.
 */
class MarketoMaUserService implements MarketoMaUserServiceInterface {

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The marketo MA API client service.
   *
   * @var \Drupal\marketo_ma\Service\MarketoMaServiceInterface
   */
  protected $marketoMaService;

  /**
   * The marketo MA API client service.
   *
   * @var \Drupal\marketo_ma\Service\MarketoMaApiClientInterface
   */
  protected $marketoMaApiClient;

  /**
   * The state storage service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * Request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxy
   */
  protected $currentUser;

  /**
   * Marketo user integration settings.
   *
   * @var \Drupal\Core\Config\ImmutableConfig|null
   */
  private $config;

  /**
   * Creates the Marketo MA user core service..
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\marketo_ma\Service\MarketoMaServiceInterface $marketo_ma
   *   The Marketo MA service.
   * @param \Drupal\marketo_ma\Service\MarketoMaApiClientInterface $api_client
   *   The marketo ma api client.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state key value store.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   Request stack.
   * @param \Drupal\Core\Session\AccountProxy $current_user
   *   The current user to see if update is triggered by self or admin.
   */
  public function __construct(ConfigFactoryInterface $config_factory, MarketoMaServiceInterface $marketo_ma, MarketoMaApiClientInterface $api_client, StateInterface $state, RequestStack $request_stack, AccountProxy $current_user) {
    $this->configFactory = $config_factory;
    $this->marketoMaService = $marketo_ma;
    $this->marketoMaApiClient = $api_client;
    $this->state = $state;
    $this->requestStack = $request_stack;
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public function config() {
    // Load config if not already loaded.
    if (!isset($this->config)) {
      $this->config = $this->configFactory->get(MarketoMaUserServiceInterface::MARKETO_MA_USER_CONFIG_NAME);
    }
    return $this->config;
  }

  /**
   * {@inheritdoc}
   */
  public function userLogin(UserInterface $account) {
    if (in_array('login', $this->config()->get('events'))) {
      $this->updateLead($account, TRUE);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function userCreate(UserInterface $account) {
    if (in_array('create', $this->config()->get('events'))) {
      $this->updateLead($account, $account->id() == $this->currentUser->id());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function userUpdate(UserInterface $account) {
    if (in_array('update', $this->config()->get('events'))) {
      $this->updateLead($account, $account->id() == $this->currentUser->id());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function updateLead(UserInterface $user, bool $sync_cookie = FALSE): ?int {
    // Get the enabled fields from the marketo ma service.
    $enabled_fields = $this->marketoMaService->getEnabledFields();

    $mapping = $this->config()->get('mapping');
    $data = [];

    foreach ($mapping as $contact_field_name => $marketo_field_id) {
      // Make sure there is a value to set and the field is still enabled.
      /** @var \Drupal\Core\Field\FieldItemInterface|null $field_item */
      $field_item = $user->get($contact_field_name)->first();
      if ($field_item && isset($enabled_fields[$marketo_field_id])) {
        // Standardize the field.
        $field = $enabled_fields[$marketo_field_id];
        // Get the field name.
        $field_name = $field->getFieldName($this->marketoMaService->trackingMethod());
        // Adds the field value to the mapped data.
        $data[$field_name] = $field_item->{$field_item->mainPropertyName()};
      }
    }

    if (!empty($data)) {
      $lead = new Lead($data);
      if ($sync_cookie) {
        $lead->setCookie($this->requestStack->getCurrentRequest()->cookies->get('_mkto_trk'));
      }
      // Let the Marketo MA module handle the rest.
      return $this->marketoMaService->updateLead($lead);
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getMarketoActivities($reset = FALSE) {
    // Reset if requested or fields have never been retrieved.
    if ($reset || $this->state->get('marketo_ma_user.activity_types', FALSE) === FALSE) {
      // Get the fields.
      $activity_types = $this->marketoMaApiClient->canConnect() ? $this->marketoMaApiClient->getActivityTypes() : [];
      // Save the field options in state.
      if (!empty($activity_types)) {
        $this->state->set('marketo_ma_user.activity_types', $activity_types);
      }
    }

    return $this->state->get('marketo_ma_user.activity_types', []);
  }

}
