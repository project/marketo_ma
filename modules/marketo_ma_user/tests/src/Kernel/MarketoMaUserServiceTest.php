<?php

namespace Drupal\Tests\marketo_ma_user\Kernel;

use Drupal\marketo_ma_user\Service\MarketoMaUserServiceInterface;

/**
 * @coversDefaultClass \Drupal\marketo_ma_user\Service\MarketoMaUserService
 *
 * @group marketo_ma_user
 */
class MarketoMaUserServiceTest extends MarketoMaUserKernelTestBase {

  /**
   * Test user service registration.
   */
  public function testServiceRegistration() {
    $this->assertTrue(\Drupal::hasService('marketo_ma.user'), 'Marketo MA User service registered');
    $service = \Drupal::service('marketo_ma.user');
    $this->assertTrue($service instanceof MarketoMaUserServiceInterface, 'The Marketo MA User service exists and implements the proper interface.');
  }

}
