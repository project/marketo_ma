<?php

namespace Drupal\Tests\marketo_ma_user\Kernel;

use Drupal\marketo_ma_user\Controller\MarketoMaUserLeadDataController;
use Drupal\user\Entity\User;

/**
 * @covers \Drupal\marketo_ma_user\Controller\MarketoMaUserLeadDataController
 * @group marketo_ma_user
 */
class MarketoMaUserControllerTest extends MarketoMaUserKernelTestBase {

  /**
   * Some based lead controller tests.
   *
   * @todo Add tests for the two controller callbacks.
   */
  public function testLeadController() {
    // Get the controller.
    $controller = MarketoMaUserLeadDataController::create($this->container);

    $account_name = $this->randomMachineName();
    $account_email = $this->randomMachineName() . '@example.com';
    // Render the lead page.
    $user = User::create([
      'uid' => 2,
      'email' => $account_email,
      'name' => $account_name,
    ]);

    // Get the page title.
    $title = (string) $controller->viewLeadTitle($user);
    // Build the output.
    $build = $controller->viewLead($user);
    // Render the printed output.
    $content = $this->render($build);

    $this->assertSame("Marketo Lead ({$account_name})", $title, 'The title contains the user name.');

    $this->assertNotEmpty($content, 'There is content.');
  }

  /**
   * Test activities controller.
   */
  public function testViewActivity() {
    // Get the controller.
    $controller = MarketoMaUserLeadDataController::create($this->container);

    $account_name = $this->randomMachineName();
    $account_email = $this->randomMachineName() . '@example.com';
    // Render the lead page.
    $user = User::create([
      'uid' => 2,
      'email' => $account_email,
      'name' => $account_name,
    ]);

    // Get the page title.
    $title = (string) $controller->viewActivityTitle($user);
    // Build the output.
    $build = $controller->viewActivity($user);
    // Render the printed output.
    $content = $this->render($build);

    $this->assertSame("Marketo Lead Activity ({$account_name})", $title, 'The title contains the user name.');

    $this->assertNotEmpty($content, 'There is content.');
    $this->assertStringContainsString('No enabled activity types enabled', $content, 'Skip rendering without enabled activities.');
    // @todo assert content with results.
  }

}
