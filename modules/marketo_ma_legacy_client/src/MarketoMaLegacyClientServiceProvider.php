<?php

namespace Drupal\marketo_ma_legacy_client;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Drupal\marketo_ma_legacy_client\Service\MarketoMaApiLegacyClient;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Replace default client with the legacy client.
 */
class MarketoMaLegacyClientServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritDoc}
   */
  public function alter(ContainerBuilder $container) {
    if ($container->hasDefinition('marketo_ma.api_client')) {
      $definition = $container->getDefinition('marketo_ma.api_client');
      if (class_exists(MarketoMaApiLegacyClient::class)) {
        // @see https://www.drupal.org/docs/drupal-apis/services-and-dependency-injection/altering-existing-services-providing-dynamic
        $definition->setClass(MarketoMaApiLegacyClient::class);
        $definition->setArguments([
          new Reference('config.factory'),
          new Reference('marketo_ma.secrets'),
          new Reference('logger.channel.marketo_ma'),
        ]);
      }
    }
  }

}
