<?php

namespace Drupal\Tests\marketo_ma_legacy_client\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\marketo_ma_legacy_client\Service\MarketoMaApiLegacyClient;

/**
 * @coversDefaultClass \Drupal\marketo_ma_legacy_client\MarketoMaLegacyClientServiceProvider
 *
 * @group marketo_ma_user
 */
class MarketoMaUserServiceTest extends KernelTestBase {

  /**
   * {@inheritDoc}
   */
  protected static $modules = [
    'marketo_ma',
    'marketo_ma_legacy_client',
  ];

  /**
   * Test user service registration.
   */
  public function testServiceRegistration() {
    $this->assertTrue(\Drupal::hasService('marketo_ma.api_client'), 'Marketo MA API client service registered');
    $service = \Drupal::service('marketo_ma.api_client');
    $this->assertTrue($service instanceof MarketoMaApiLegacyClient, 'The Marketo MA API client service exists and is the legacy service.');
  }

}
