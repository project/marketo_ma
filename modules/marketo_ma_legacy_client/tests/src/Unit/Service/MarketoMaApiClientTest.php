<?php

namespace Drupal\Tests\marketo_ma_legacy_client\Unit\Service;

use CSD\Marketo\Client as GuzzleClient;
use CSD\Marketo\Response;
use CSD\Marketo\Response\AddOrRemoveLeadsToListResponse;
use CSD\Marketo\Response\CreateOrUpdateLeadsResponse;
use CSD\Marketo\Response\DeleteLeadResponse;
use CSD\Marketo\Response\GetLeadResponse;
use CSD\Marketo\Response\GetPagingToken;
use Drupal\marketo_ma\Lead;
use Drupal\marketo_ma_legacy_client\Service\MarketoMaApiLegacyClient;
use Drupal\Tests\UnitTestCase;
use Prophecy\Argument;
use Psr\Log\LoggerInterface;

/**
 * Test legacy API client.
 *
 * @coversDefaultClass \Drupal\marketo_ma_legacy_client\Service\MarketoMaApiLegacyClient
 * @group marketo_ma
 */
class MarketoMaApiClientTest extends UnitTestCase {

  /**
   * Mocked CSD Marketo client.
   *
   * @var \CSD\Marketo\Client|\Prophecy\Prophecy\ObjectProphecy
   */
  protected $realClient;

  /**
   * The marketo_ma rest client service.
   *
   * @var \Drupal\marketo_ma\Service\MarketoMaApiClientInterface
   */
  protected $marketoMaApiClient;

  /**
   * {@inheritDoc}
   */
  public function setUp(): void {
    parent::setUp();
    if (!class_exists(GuzzleClient::class)) {
      $this->markTestSkipped('Legacy client missing. Test cannot be run.');
    }
    // Get the API client service.
    $this->realClient = $this->prophesize(GuzzleClient::class);
  }

  /**
   * @covers ::canConnect
   * @dataProvider provideConnect
   */
  public function testApiClientCanConnect($config, $expect) {
    $this->assertEquals($expect, $this->getClient($config)->canConnect());
  }

  /**
   * @covers ::getFields
   */
  public function testGetFields() {
    $field1 = ['rest' => ['name' => 'foo']];
    $field2 = ['rest' => ['name' => 'bar']];
    $response = new Response([
      'result' => [
        $field1,
        $field2,
      ],
    ]);
    $this->realClient->describeLeads()
      ->shouldBeCalledOnce()
      ->willReturn($response);
    $fields = $this->getClient([])->getFields();
    // Assert fields match original fields plus default name. Why?
    $this->assertEquals([
      $field1,
      $field2,
    ], $fields);
  }

  /**
   * @covers ::getActivityTypes
   */
  public function testGetActivityTypes() {
    $expected_activities = [];
    $response = new Response([
      'result' => $expected_activities,
    ]);
    $this->markTestIncomplete('This does not exist on the class and so can not be mocked.');
    $this->realClient->getActivityTypes()
      ->shouldBeCalledOnce()
      ->willReturn($response);
    $activity_types = $this->getClient([])->getActivityTypes();
    // Assert fields match original fields plus default name. Why?
    $this->assertEquals($expected_activities, $activity_types);
  }

  /**
   * @covers ::getLeadById
   */
  public function testGetLeadById() {
    $lead = ['asdf' => 123];
    $response = new GetLeadResponse([
      'success' => TRUE,
      'result' => [$lead],
    ]);
    $this->realClient->getLead(123)
      ->shouldBeCalledOnce()
      ->willReturn($response);
    $activity_types = $this->getClient([])->getLeadById(123);
    // Assert fields match original fields plus default name. Why?
    $this->assertEquals(new Lead($lead), $activity_types);
  }

  /**
   * @covers ::getLeadByEmail
   */
  public function testGetLeadByEmail() {
    $lead = ['asdf' => 123];
    $response = new GetLeadResponse([
      'success' => TRUE,
      'result' => [$lead],
    ]);
    $this->realClient->getLeadByFilterType('email', 'test@example.com')
      ->shouldBeCalledOnce()
      ->willReturn($response);
    $activity_types = $this->getClient([])->getLeadByEmail('test@example.com');
    // Assert fields match original fields plus default name. Why?
    $this->assertEquals(new Lead($lead), $activity_types);
  }

  /**
   * @covers ::getLeadActivity
   */
  public function testGetLeadActivity() {
    $lead = new Lead(['id' => 123]);
    $this->realClient->getPagingToken(Argument::type('string'))
      ->shouldBeCalledOnce()
      ->willReturn(new GetPagingToken([
        'success' => TRUE,
        'nextPageToken' => 'asdf1234',
      ]));
    $result = ['test' => 123];
    $response = new Response([
      'result' => $result,
    ]);
    $this->realClient->getLeadActivity('asdf1234', 123, 1234)
      ->shouldBeCalledOnce()
      ->willReturn($response);
    $activity_types = $this->getClient([])->getLeadActivity($lead, 1234);
    // Assert fields match original fields plus default name. Why?
    $this->assertEquals($result, $activity_types);
  }

  /**
   * @covers ::syncLead
   */
  public function testSyncLead() {
    $lead_data = ['id' => 543, 'name' => 'test'];
    $lead = new Lead($lead_data);
    $this->realClient->createOrUpdateLeads([$lead_data], 'email', [])
      ->shouldBeCalledOnce()
      ->willReturn(new CreateOrUpdateLeadsResponse(['result' => [$lead_data]]));
    $this->assertEquals($lead_data['id'], $this->getClient([])->syncLead($lead));
  }

  /**
   * @covers ::deleteLead
   */
  public function testDeleteLead() {
    $leads = [123, 234, 345];
    $this->realClient->deleteLead($leads)
      ->shouldBeCalledOnce()
      ->willReturn(new DeleteLeadResponse(['result' => TRUE]));
    $this->assertTrue($this->getClient([])->deleteLead($leads));
  }

  /**
   * @covers ::addLeadsToList
   * @covers ::getLeadsIds
   */
  public function testAddLeadsToList() {
    $leads_raw = [123, 234, 345];
    $leads = array_map(function ($id) {
      return new Lead(['id' => $id]);
    }, $leads_raw);

    $options = ['myoption' => 'my value'];
    $this->realClient->addLeadsToList(789, implode(',', $leads_raw), $options)
      ->shouldBeCalledOnce()
      ->willReturn(new AddOrRemoveLeadsToListResponse(['result' => TRUE]));
    $this->assertTrue($this->getClient([])->addLeadsToList(789, $leads, $options));

    $this->realClient->addLeadsToList(789, implode(',', $leads_raw), [])
      ->shouldBeCalledOnce()
      ->willReturn(new AddOrRemoveLeadsToListResponse(['result' => TRUE]));
    $this->assertTrue($this->getClient([])->addLeadsToList(789, $leads));
    // @todo Add a test for exception handling in getLeadsIds().
  }

  /**
   * @covers ::addLeadToListByEmail
   */
  public function testAddLeadToListByEmail() {
    $lead_data = ['id' => 543, 'name' => 'test'];
    $lead_response = new GetLeadResponse([
      'success' => TRUE,
      'result' => [$lead_data],
    ]);
    $this->realClient->getLeadByFilterType('email', 'test@example.com')
      ->willReturn($lead_response);

    $this->realClient->createOrUpdateLeads(Argument::any(), Argument::any(), Argument::any())
      ->willReturn(new CreateOrUpdateLeadsResponse(['result' => []]));

    $options = ['myoption' => 'my value'];
    $this->realClient->addLeadsToList(789, '543', $options)
      ->shouldBeCalledOnce()
      ->willReturn(new AddOrRemoveLeadsToListResponse(['result' => TRUE]));
    $this->getClient([])->addLeadToListByEmail(789, 'test@example.com', $options);

    $this->realClient->addLeadsToList(789, '543', [])
      ->shouldBeCalledOnce()
      ->willReturn(new AddOrRemoveLeadsToListResponse(['result' => TRUE]));
    $this->getClient([])->addLeadToListByEmail(789, 'test@example.com');
  }

  /**
   * Create a client with some mock helpers and configured.
   *
   * @param array $config
   *   Raw client config.
   *
   * @return \Drupal\marketo_ma_legacy_client\Service\MarketoMaApiLegacyClient
   *   A legacy api client for testing.
   */
  private function getClient(array $config) {

    return new class (
      $config,
      $this->prophesize(LoggerInterface::class)->reveal(),
      $this->realClient->reveal()) extends
      MarketoMaApiLegacyClient {

      /**
       * Create mocked client.
       *
       * @param array $config
       *   Prepopulated configuration.
       * @param \Psr\Log\LoggerInterface $logger
       *   Logger.
       * @param \CSD\Marketo\Client $apiClient
       *   Client.
       */
      public function __construct(
        array $config,
        LoggerInterface $logger,
        GuzzleClient $apiClient
      ) {
        $this->clientConfig = $config;
        $this->logger = $logger;
        $this->client = $apiClient;
      }

    };
  }

  /**
   * Provide connection options.
   *
   * @return array[]
   *   List of different configuration options.
   */
  public function provideConnect() {
    return [
      [
        [
          'munchkin_id' => 'a',
          'client_id' => 'a',
        ],
        TRUE,
      ],
      [
        [],
        FALSE,
      ],
    ];
  }

}
