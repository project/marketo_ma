<?php

namespace Drupal\marketo_ma_contact\Hooks;

use Drupal\contact\MessageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\marketo_ma\Lead;
use Drupal\marketo_ma\Service\MarketoMaServiceInterface;

/**
 * Contact form message hook helper.
 */
class ContactMessageInsert {

  /**
   * Marketo integration service.
   *
   * @var \Drupal\marketo_ma\Service\MarketoMaServiceInterface
   */
  protected $marketoMaService;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Stores the loaded mapping configuration.
   *
   * @var array|null
   */
  protected $mappingConfiguration;

  /**
   * Creates a new ContactMessageInsert instance.
   *
   * @param \Drupal\marketo_ma\Service\MarketoMaServiceInterface $marketo_ma_service
   *   Marketo integration service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager service.
   */
  public function __construct(
    MarketoMaServiceInterface $marketo_ma_service,
    EntityTypeManagerInterface $entityTypeManager
  ) {
    $this->marketoMaService = $marketo_ma_service;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * Handle message insertion.
   *
   * @param \Drupal\contact\MessageInterface $message
   *   Message being inserted.
   */
  public function contactMessageInsert(MessageInterface $message) {
    if ($this->isTrackingEnabled($message->bundle())) {
      $data = $this->determineMappedData($message);
      $this->marketoMaService->updateLead(new Lead($data));
    }
  }

  /**
   * Loads the mapping configuration for a specific contact form.
   *
   * @param string $contact_form_id
   *   The contact form id.
   *
   * @return array
   *   Field mapping for the form.
   */
  protected function loadMappingConfiguration($contact_form_id) {
    if (!isset($this->mappingConfiguration)) {
      /** @var \Drupal\contact\ContactFormInterface $contact_form */
      $contact_form = $this->entityTypeManager->getStorage('contact_form')->load($contact_form_id);
      $this->mappingConfiguration = $contact_form->getThirdPartySetting('marketo_ma_contact', 'mapping', []);
    }
    return $this->mappingConfiguration;
  }

  /**
   * Determines whether some marketo tracking is enabled.
   *
   * @param string $contact_form_id
   *   The contact form id.
   *
   * @return bool
   *   TRUE if marketo tracking is enabled.
   */
  protected function isTrackingEnabled($contact_form_id) {
    /** @var \Drupal\contact\Entity\ContactForm $contact_form */
    $contact_form = $this->entityTypeManager->getStorage('contact_form')->load($contact_form_id);
    return ($contact_form->getThirdPartySetting('marketo_ma_contact', 'enabled', 0) === 1
      && !empty($this->loadMappingConfiguration($contact_form_id)));
  }

  /**
   * Determines data mapping from the contact form to marketo fields.
   *
   * @param \Drupal\contact\MessageInterface $message
   *   A contact message.
   *
   * @return array
   *   The mapping data, keyed by marketo field name.
   */
  protected function determineMappedData(MessageInterface $message) {
    $enables_fields = $this->marketoMaService->getEnabledFields();

    $mapping = $this->loadMappingConfiguration($message->bundle());
    $data = [];

    foreach ($mapping as $contact_field_name => $marketo_field_id) {
      // Make sure there is a value to set and the field is still enabled.
      /** @var \Drupal\Core\Field\FieldItemInterface|false $field_item */
      $field_item = $message->get($contact_field_name)->first();
      if ($field_item && isset($enables_fields[$marketo_field_id])) {
        // Get the field name.
        $field = $enables_fields[$marketo_field_id];
        $field_name = $field->getFieldName($this->marketoMaService->trackingMethod());
        // Adds the field value to the mapped data.
        $data[$field_name] = $field_item->{$field_item->mainPropertyName()};
      }
    }
    return $data;
  }

}
