<?php

namespace Drupal\Tests\marketo_ma_contact\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\marketo_ma\Traits\TestFieldsTrait;

/**
 * Base class for testing Contact integrations.
 */
abstract class MarketoMaContactTestBase extends BrowserTestBase {

  use TestFieldsTrait;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['marketo_ma_contact', 'marketo_mock_client'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Populate field data.
    $this->createSampleFields();
  }

}
