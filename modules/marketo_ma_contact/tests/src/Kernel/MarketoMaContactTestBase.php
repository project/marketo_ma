<?php

namespace Drupal\Tests\marketo_ma_contact\Kernel;

use Drupal\contact\Entity\ContactForm;
use Drupal\KernelTests\KernelTestBase;
use Drupal\marketo_ma\Service\MarketoMaServiceInterface;
use Drupal\Tests\marketo_ma\Traits\TestFieldsTrait;

/**
 * Base class for testing Contact integrations.
 */
abstract class MarketoMaContactTestBase extends KernelTestBase {

  use TestFieldsTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'marketo_ma',
    'user',
    'contact',
    'contact_storage',
    'marketo_ma_contact',
    'marketo_mock_client',
  ];

  /**
   * {@inheritDoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installConfig(['marketo_ma']);
    // Install module schema.
    $this->installSchema('marketo_ma', 'marketo_ma_lead_fields');

    // Populate field data.
    $this->createSampleFields();

    \Drupal::configFactory()->getEditable(MarketoMaServiceInterface::MARKETO_MA_CONFIG_NAME)
      ->set('tracking_method', MarketoMaServiceInterface::TRACKING_METHOD_API)
      ->save();

    /** @var \Drupal\contact\ContactFormInterface $contact_form */
    $contact_form = ContactForm::create([
      'label' => 'test contact',
      'id' => 'test_contact',
      'recipients' => ['foo@example.com'],
      'contact_storage_preview' => FALSE,
    ]);

    $contact_form->setThirdPartySetting('marketo_ma_contact', 'mapping', [
      'name' => 'firstName',
      'mail' => 'email',
    ]);
    $contact_form->setThirdPartySetting('marketo_ma_contact', 'enabled', 1);

    $contact_form->save();

    $this->installEntitySchema('contact_message');
  }

}
