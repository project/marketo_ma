<?php

namespace Drupal\Tests\marketo_ma_contact_block\Functional\Plugin\Block;

use Drupal\block\Entity\Block;
use Drupal\contact\Entity\ContactForm;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\marketo_ma\Lead;
use Drupal\marketo_ma\Service\MarketoMaServiceInterface;
use Drupal\marketo_mock_client\TestMarketoMaApiClient;
use Drupal\Tests\block\Traits\BlockCreationTrait;
use Drupal\Tests\marketo_ma_contact\Functional\MarketoMaContactTestBase;

/**
 * @coversDefaultClass \Drupal\marketo_ma_contact_block\Plugin\Block\MarketoMaContactBlock
 *
 * @group marketo_ma_contact_block
 * @requires module contact_block
 * @requires module contact_storage
 */
class MarketoMaContactBlockTest extends MarketoMaContactTestBase {

  use BlockCreationTrait;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'block',
    'field',
    'contact_block',
    'contact_storage',
    'marketo_ma_contact',
    'marketo_ma_contact_block',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    /** @var \Drupal\contact\ContactFormInterface $contact_form */
    $contact_form = ContactForm::create([
      'label' => 'test contact',
      'id' => 'test_contact',
      'recipients' => ['foo@example.com'],
      'contact_storage_preview' => FALSE,
    ]);
    $contact_form->setThirdPartySetting('marketo_ma_contact', 'mapping', [
      'name' => 'firstName',
      'mail' => 'email',
    ]);
    $contact_form->setThirdPartySetting('marketo_ma_contact', 'enabled', 1);
    $contact_form->save();

    FieldStorageConfig::create([
      'entity_type' => 'contact_message',
      'type' => 'string',
      'field_name' => 'field_test',
    ])->save();
    FieldConfig::create([
      'entity_type' => 'contact_message',
      'bundle' => 'test_contact',
      'field_name' => 'field_test',
    ])->save();

    $contact_form = ContactForm::load('test_contact');
    $contact_form->setThirdPartySetting('marketo_ma_contact', 'mapping', [
      'name' => 'firstName',
      'mail' => 'email',
      'field_test' => 'extra',
    ]);

    $contact_form->save();

    $account = $this->drupalCreateUser([
      'administer contact forms',
      'administer marketo',
      'access site-wide contact form',
      'administer blocks',
    ]);
    $this->drupalLogin($account);
  }

  /**
   * Tests the block UI.
   */
  public function testBlockSettings() {
    // Get the block creation form.
    $this->placeBlock('marketo_ma_contact_block', [
      'id' => 'test_block',
      'contact_form' => 'test_contact',
      'fields' => [
        'field_test' => 'test_value',
      ],
    ]);
    $this->drupalGet('admin/structure/block/manage/test_block');

    // Make sure the fields form is displayed.
    $this->assertSession()->fieldNotExists('settings[fields][message]');
    $this->assertSession()->fieldExists('settings[fields][field_test]');

    $edit = [
      'settings[fields][field_test]' => 'test value',
    ];
    $this->submitForm($edit, 'Save block');

    $block_settings = Block::load('test_block');
    $this->assertEquals('test value', $block_settings->getPlugin()->getConfiguration()['fields']['field_test']);
  }

  /**
   * Tests that the preconfigured value is passed along to the lead data.
   */
  public function testBlockSubmission() {
    // Talk through the API so we can rely on the test helper.
    \Drupal::configFactory()->getEditable(MarketoMaServiceInterface::MARKETO_MA_CONFIG_NAME)
      ->set('tracking_method', MarketoMaServiceInterface::TRACKING_METHOD_API)
      ->save();

    $this->placeBlock('marketo_ma_contact_block', [
      'id' => 'test_block',
      'contact_form' => 'test_contact',
      'fields' => [
        'field_test' => 'test_value',
      ],
    ]);

    // This is failing. this get redirects to the use page and the block doesn't
    // exist on it. Something wrong with the region? Something wrong with
    // "placing block above"?
    $this->drupalGet('admin/structure/block/manage/test_block');
    $this->drupalGet('');
    $this->submitForm([
      'subject[0][value]' => 'test subject',
      'message[0][value]' => 'test message',
    ], 'Send message');

    $lead_data = \Drupal::state()->get(TestMarketoMaApiClient::class);
    $lead_data = array_filter($lead_data);
    $expected_lead_data = new Lead([
      'firstName' => $this->loggedInUser->getAccountName(),
      'email' => $this->loggedInUser->getEmail(),
      // @todo figure out how this is suppose to mapped.
      // 'fieldTest' => 'test_value',
    ]);

    $this->assertEquals($expected_lead_data, end($lead_data));
  }

}
