<?php

namespace Drupal\Tests\marketo_ma_webform\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\marketo_ma\Traits\TestFieldsTrait;
use Drupal\webform\Element\WebformSelectOther;
use Drupal\webform\Entity\Webform;

/**
 * Marketo MA Webform Handler Form tests.
 *
 * @group marketo_ma_webform
 */
class MarketoMaWebformHandlerFormTest extends BrowserTestBase {

  use TestFieldsTrait;

  /**
   * {@inheritDoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritDoc}
   */
  protected static $modules = [
    'webform',
    'marketo_ma_webform',
    'webform_test_handler',
  ];

  /**
   * Webforms to load.
   *
   * @var array
   */
  protected static $testWebforms = [];

  /**
   * {@inheritDoc}
   */
  public function setUp(): void {
    parent::setUp();
    $this->createSampleFields();
  }

  /**
   * Tests webform handler plugin.
   */
  public function testWebformHandlerAdmin() {
    // Setup assert session.
    $assert = $this->assertSession();
    // Login.
    $this->drupalLogin($this->rootUser);
    // Load test form.
    $form = Webform::load('contact');
    $this->assertTrue((bool) $form, 'form exists...');
    // Add handler to web form.
    $this->drupalGet('admin/structure/webform/manage/contact/handlers/add/marketo_ma');
    $assert->pageTextContains('Sends a webform submission via Marketo MA.');
    $assert->pageTextContains('Webform to Marketo MA Lead mapping');

    // Something is wrong with the config schema. This is legit broken.
    $this->submitForm(['handler_id' => 'marketo_ma', 'status' => TRUE], 'Save');
    $assert->pageTextContains('Sends a webform submission via Marketo MA.');

    // Map an enabled field using "other."
    // Using "other" to skip having to mock or load Marketo fields.
    $this->drupalGet('admin/structure/webform/manage/contact/handlers/marketo_ma/edit');
    $this->submitForm(
      [
        'handler_id' => 'marketo_ma',
        'status' => TRUE,
        'settings[marketo_ma_mapping][name][select]' => 'firstName',
        'settings[marketo_ma_mapping][email][select]' => WebformSelectOther::OTHER_OPTION,
        'settings[marketo_ma_mapping][email][other]' => 'foo',
      ],
      'Save'
    );
    $this->drupalGet('admin/structure/webform/manage/contact/handlers/marketo_ma/edit');
    $assert->fieldValueEquals('settings[marketo_ma_mapping][name][select]', 'firstName');
    $assert->fieldValueEquals('settings[marketo_ma_mapping][email][select]', WebformSelectOther::OTHER_OPTION);
    $assert->fieldValueEquals('settings[marketo_ma_mapping][email][other]', 'foo');
  }

}
