<?php

namespace Drupal\Tests\marketo_ma_webform\Unit\Event;

use Drupal\marketo_ma\Exception\ProcessingException;
use Drupal\marketo_ma_webform\Event\SubmissionFailure;
use Drupal\Tests\UnitTestCase;

/**
 * @coversDefaultClass \Drupal\marketo_ma_webform\Event\SubmissionFailure
 */
class SubmissionFailureTest extends UnitTestCase {

  /**
   * @covers ::__construct
   * @covers ::getFormId
   */
  public function testGetFormId() {
    $sot = new SubmissionFailure('123', ['foo' => 'bar'], new ProcessingException());
    $this->assertSame('123', $sot->getFormId());
  }

  /**
   * @covers ::__construct
   * @covers ::getData
   */
  public function testGetData() {
    $sot = new SubmissionFailure('123', ['foo' => 'bar'], new ProcessingException());
    $this->assertSame(['foo' => 'bar'], $sot->getData());
  }

  /**
   * @covers ::__construct
   * @covers ::getException
   */
  public function testGetException() {
    $e = new ProcessingException();
    $sot = new SubmissionFailure('123', ['foo' => 'bar'], $e);
    $this->assertSame($e, $sot->getException());
  }

}
