<?php

namespace Drupal\Tests\marketo_ma_webform\Unit\Plugin\WebformHandler;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\marketo_ma\Lead;
use Drupal\marketo_ma\MarketoFieldDefinition;
use Drupal\marketo_ma\Service\MarketoMaServiceInterface;
use Drupal\marketo_ma_webform\Plugin\WebformHandler\MarketoMaWebformHandler;
use Drupal\Tests\UnitTestCase;
use Drupal\webform\Entity\Webform;
use Drupal\webform\WebformSubmissionConditionsValidatorInterface;
use Drupal\webform\WebformSubmissionInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * MarketoMaWebformHandler tests.
 *
 * @group marketo_ma_webform
 * @coversDefaultClass \Drupal\marketo_ma_webform\Plugin\WebformHandler\MarketoMaWebformHandler
 */
class MarketoMaWebformHandlerTest extends UnitTestCase {

  /**
   * Mock logger factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface|\Prophecy\Prophecy\ObjectProphecy
   */
  protected $loggerFactory;

  /**
   * Mock config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface|\Prophecy\Prophecy\ObjectProphecy
   */
  protected $configFactory;

  /**
   * Mock entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface|\Prophecy\Prophecy\ObjectProphecy
   */
  protected $entityTypeManager;

  /**
   * Mock condition validator.
   *
   * @var \Drupal\webform\WebformSubmissionConditionsValidatorInterface|\Prophecy\Prophecy\ObjectProphecy
   */
  protected $conditionsValidator;

  /**
   * Test config.
   *
   * @var array[]
   */
  protected $testConfig;

  /**
   * Mock marketo service.
   *
   * @var \Drupal\marketo_ma\Service\MarketoMaServiceInterface|\Prophecy\Prophecy\ObjectProphecy
   */
  protected $marketoService;

  /**
   * {@inheritDoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->loggerFactory = $this->prophesize(LoggerChannelFactoryInterface::class);
    $this->configFactory = $this->prophesize(ConfigFactoryInterface::class);
    $this->entityTypeManager = $this->prophesize(EntityTypeManagerInterface::class);
    $this->conditionsValidator = $this->prophesize(WebformSubmissionConditionsValidatorInterface::class);
    $this->marketoService = $this->prophesize(MarketoMaServiceInterface::class);
    $this->marketoService->getMarketoFields()->willReturn($this->getMarketoFields());
    $this->testConfig = [
      'settings' => [
        'formid' => 123,
        'marketo_ma_list' => 'test_list',
        'program_name' => 'some program',
        'marketo_ma_mapping' => [
          'my_form_element_1' => 44,
          'my_form_element_2' => 41,
          'my_form_element_3' => 43,
        ],
      ],
    ];
  }

  /**
   * Helper to build a functional handler.
   */
  private function getHandler() {
    return new class(
      [
        'settings' => [
          'marketo_ma_mapping' => [
            'my_form_element_1' => 44,
            'my_form_element_2' => 41,
            'my_form_element_3' => 43,
          ],
        ],
      ],
      'marketo_ma',
      [
        'label' => 'Marketo Webform',
      ],
      $this->loggerFactory->reveal(),
      $this->configFactory->reveal(),
      $this->entityTypeManager->reveal(),
      $this->conditionsValidator->reveal(),
      $this->marketoService->reveal(),
      new RequestStack()
    ) extends MarketoMaWebformHandler {

      /**
       * {@inheritDoc}
       */
      public function getLead(WebformSubmissionInterface $webform_submission): Lead {
        return parent::getlead($webform_submission);
      }

      /**
       * {@inheritDoc}
       */
      public function getMappedData(WebformSubmissionInterface $webform_submission): array {
        return parent::getMappedData($webform_submission);
      }

    };
  }

  /**
   * @covers ::__construct
   * @covers ::create
   */
  public function testCreate() {
    $container = new ContainerBuilder();
    $container->set('logger.factory', $this->loggerFactory->reveal());
    $container->set('config.factory', $this->configFactory->reveal());
    $container->set('entity_type.manager', $this->entityTypeManager->reveal());
    $container->set('webform_submission.conditions_validator', $this->conditionsValidator->reveal());
    $container->set('marketo_ma', $this->marketoService->reveal());
    $container->set('request_stack', new RequestStack());
    $sot = MarketoMaWebformHandler::create($container, $this->testConfig, 'marketo_ma', [
      'label' => 'Marketo Webform',
    ]);
    $this->assertInstanceOf(MarketoMaWebformHandler::class, $sot);
    // Ignore the stuff webform adds and just make sure our settings are there.
    $this->assertSame($this->testConfig['settings'], $sot->getConfiguration()['settings']);
  }

  /**
   * @covers ::getSummary
   */
  public function testGetSummary() {
    $sot = $this->getHandler();
    $this->assertSame($sot->getSettings(), $sot->getSummary()['#settings']);
    // Assert some stuff is there to confirm we're merging the parent summary.
    $this->assertSame('webform_handler_marketo_ma_summary', $sot->getSummary()['#theme']);
  }

  /**
   * @covers ::getLead
   */
  public function testGetLeadProf() {
    $webform_submission_array = [
      'my_form_element_1' => 'foo',
      'my_form_element_2' => 'bar',
      'my_form_element_3' => 'baz',
    ];
    $sot = $this->getHandler();
    $lead = $sot->getLead($this->getMarketoSubmission($webform_submission_array));
    $this->assertEquals([
      'email' => 'foo',
      'firstName' => 'bar',
      'lastName' => 'baz',
    ], $lead->data());
  }

  /**
   * @covers ::getMappedData
   */
  public function testGetMappedData() {
    $webform_submission_array = [
      'my_form_element_1' => 'foo',
      'my_form_element_2' => 'bar',
      'my_form_element_3' => 'baz',
    ];
    $this->assertEquals(
      [
        'email' => 'foo',
        'firstName' => 'bar',
        'lastName' => 'baz',
      ],
      $this->getHandler()->getMappedData($this->getMarketoSubmission($webform_submission_array))
    );
  }

  /**
   * @covers ::postSave
   */
  public function testPostSave() {
    $sot = $this->getHandler();
    $this->markTestIncomplete('This requires quite a bit more setup. Functional webform entity, etc.');
    $sot->postSave(
      $this->getMarketoSubmission([
        'my_form_element_1' => 'foo',
        'my_form_element_2' => 'bar',
        'my_form_element_3' => 'baz',
      ])
    );
  }

  /**
   * Helper method to get mock form submission.
   *
   * @param array $webform_submission_array
   *   Submission data.
   *
   * @return \Drupal\webform\WebformSubmissionInterface|\Prophecy\Prophecy\ObjectProphecy
   *   Mock form submission.
   */
  private function getMarketoSubmission(array $webform_submission_array) {
    $webform_submission = $this->prophesize(WebformSubmissionInterface::class);
    $webform_submission->toArray(TRUE)->willReturn(['data' => $webform_submission_array]);
    $webform = $this->prophesize(Webform::class);
    $webform->getElementsDecodedAndFlattened()->willReturn([]);
    $webform_submission->getWebform()->willReturn($webform->reveal());
    return $webform_submission->reveal();
  }

  /**
   * Convert raw data to Native field data objects.
   *
   * @return \Drupal\marketo_ma\MarketoFieldDefinition[]
   *   List of mock marketo field definitions.
   */
  private function getMarketoFields() {
    return array_map(function ($field_data) {
      return new MarketoFieldDefinition($field_data);
    }, $this->getMarketoFieldsData());
  }

  /**
   * Get a list of mock marketo fields.
   *
   * @return array[]
   *   Mock marketo field data.
   */
  private function getMarketoFieldsData() {
    return [
      41 => [
        'id' => 41,
        'displayName' => 'First Name',
        'dataType' => 'string',
        'length' => '255',
        'restName' => 'firstName',
        'restReadOnly' => 0,
        'soapName' => 'FirstName',
        'soapReadOnly' => 0,
      ],
      43 => [
        'id' => 43,
        'displayName' => 'Last Name',
        'dataType' => 'string',
        'length' => '255',
        'restName' => 'lastName',
        'restReadOnly' => 0,
        'soapName' => 'LastName',
        'soapReadOnly' => 0,
      ],
      44 => [
        'id' => 44,
        'displayName' => 'Email Address',
        'dataType' => 'email',
        'length' => '255',
        'restName' => 'email',
        'restReadOnly' => 0,
        'soapName' => 'Email',
        'soapReadOnly' => 0,
      ],
      2565 => [
        'id' => 2565,
        'displayName' => 'Additional Comments:',
        'dataType' => 'text',
        'length' => '',
        'restName' => 'Additional_Comments',
        'restReadOnly' => 0,
        'soapName' => 'Additional_Comments',
        'soapReadOnly' => 0,
      ],
    ];
  }

}
