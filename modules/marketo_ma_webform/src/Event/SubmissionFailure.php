<?php

namespace Drupal\marketo_ma_webform\Event;

use Drupal\marketo_ma\Exception\ProcessingException;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Event triggered when a form submission can't be handled.
 */
class SubmissionFailure extends Event {

  /**
   * Marketo Form ID.
   *
   * @var string
   */
  private $formId;

  /**
   * Field data to submit to the marketo form.
   *
   * @var array<string, string>
   */
  private $data;

  /**
   * The triggering exception.
   *
   * @var \Drupal\marketo_ma\Exception\ProcessingException
   */
  private $exception;

  /**
   * SubmissionFailure Constructor.
   *
   * @param string $form_id
   *   Marketo Form ID.
   * @param array<string, string> $data
   *   Field data to submit to the marketo form.
   * @param \Drupal\marketo_ma\Exception\ProcessingException $exception
   *   The triggering exception.
   */
  public function __construct(string $form_id, array $data, ProcessingException $exception) {
    $this->formId = $form_id;
    $this->data = $data;
    $this->exception = $exception;
  }

  /**
   * Get associated Marketo form ID.
   *
   * @return string
   *   Marketo form ID.
   */
  public function getFormId(): string {
    return $this->formId;
  }

  /**
   * Get submitted data.
   *
   * @return array<string, string>
   *   Submitted data.
   */
  public function getData(): array {
    return $this->data;
  }

  /**
   * Get the triggering exception.
   *
   * @return \Drupal\marketo_ma\Exception\ProcessingException
   *   Triggering exception.
   */
  public function getException(): ProcessingException {
    return $this->exception;
  }

}
