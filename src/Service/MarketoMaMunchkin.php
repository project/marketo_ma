<?php

namespace Drupal\marketo_ma\Service;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\marketo_ma\Lead;
use Drupal\marketo_ma\Secrets\SecretsInterface;

/**
 * The marketo MA munchkin service (marketo_ma.munchkin).
 */
class MarketoMaMunchkin implements MarketoMaMunchkinInterface {

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Configuration secrets.
   *
   * @var \Drupal\marketo_ma\Secrets\SecretsInterface
   */
  protected $secrets;

  /**
   * The route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * Marketo settings configuration.
   *
   * @var \Drupal\Core\Config\ImmutableConfig|null
   */
  protected $config;

  /**
   * Creates the Marketo API client wrapper service.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config factory for looking up config.
   * @param \Drupal\marketo_ma\Secrets\SecretsInterface $secrets
   *   Client configuration secrets.
   */
  public function __construct(ConfigFactoryInterface $config_factory, SecretsInterface $secrets) {
    $this->configFactory = $config_factory;
    $this->secrets = $secrets;
  }

  /**
   * {@inheritdoc}
   */
  public function config() {
    // Load config if not already loaded.
    if (empty($this->config)) {
      $this->config = $this->configFactory->get(MarketoMaServiceInterface::MARKETO_MA_CONFIG_NAME);
    }

    return $this->config;
  }

  /**
   * {@inheritdoc}
   */
  public function getAccountId() {
    return $this->config()->get('munchkin.account_id');
  }

  /**
   * {@inheritdoc}
   */
  public function getInitParams() {
    $config = $this->config();
    $marketo_ma_munchkin_partition = trim($config->get('munchkin.partition'));
    $marketo_ma_munchkin_altIds = trim($config->get('munchkin.altIds'));
    $marketo_ma_munchkin_cookieLifeDays = trim($config->get('munchkin.cookieLifeDays'));
    $marketo_ma_munchkin_clickTime = trim($config->get('munchkin.clickTime'));
    $marketo_ma_munchkin_cookieAnon = trim($config->get('munchkin.cookieAnon'));
    $marketo_ma_munchkin_domainLevel = trim($config->get('munchkin.domainLevel'));
    $marketo_ma_munchkin_disableClickDelay = trim($config->get('munchkin.disableClickDelay'));
    $marketo_ma_munchkin_asyncOnly = trim($config->get('munchkin.asyncOnly'));

    $initParams = [];
    if ($marketo_ma_munchkin_partition != '') {
      $initParams['wsInfo'] = $marketo_ma_munchkin_partition;
    }
    if ($marketo_ma_munchkin_altIds != '') {
      $altIds = preg_split("/,\s*/", $marketo_ma_munchkin_altIds);
      $initParams['altIds'] = $altIds;
    }
    if ($marketo_ma_munchkin_cookieLifeDays != '') {
      $initParams['cookieLifeDays'] = $marketo_ma_munchkin_cookieLifeDays;
    }
    if ($marketo_ma_munchkin_clickTime != '') {
      $initParams['clickTime'] = $marketo_ma_munchkin_clickTime;
    }
    if ($marketo_ma_munchkin_cookieAnon != '') {
      $initParams['cookieAnon'] = $marketo_ma_munchkin_cookieAnon;
    }
    if ($marketo_ma_munchkin_domainLevel != '') {
      $initParams['domainLevel'] = $marketo_ma_munchkin_domainLevel;
    }
    if ($marketo_ma_munchkin_disableClickDelay != '') {
      $initParams['disableClickDelay'] = $marketo_ma_munchkin_disableClickDelay;
    }
    if ($marketo_ma_munchkin_asyncOnly != '') {
      $initParams['asyncOnly'] = $marketo_ma_munchkin_asyncOnly;
    }

    return $initParams;
  }

  /**
   * {@inheritdoc}
   */
  public function getLibrary() {
    return $this->config()->get('munchkin.javascript_library');
  }

  /**
   * {@inheritdoc}
   */
  public function isConfigured() {
    return (!empty($this->secrets->getMunchkinApiKey())
      && !empty($this->getAccountId())
      && !empty($this->getLibrary())
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getAction($action_type, Lead $lead, $args = []) {
    if ($action_type === MarketoMaMunchkinInterface::ACTION_ASSOCIATE_LEAD && !empty($lead->getEmail())) {
      // The `associateLead` action requires the email and signing.
      return [
        'action' => $action_type,
        'data' => $lead->data(),
        'hash' => hash('sha1', $this->secrets->getMunchkinApiKey() . $lead->getEmail()),
      ];
    }
    else {
      // The cookie is used for identification. Only args are required.
      return [
        'action' => $action_type,
        'data' => $args,
      ];
    }
  }

}
