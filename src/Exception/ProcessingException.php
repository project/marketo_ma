<?php

namespace Drupal\marketo_ma\Exception;

/**
 * An exception thrown by a failure to processes a service request.
 *
 * Most service methods can have their processing delayed either by queueing or
 * by pushing to the browser in the case of munchkin. This means that we can't
 * rely on falsy return to convey a failure to process a request. In this case,
 * throwing this exception allows generic wrapping as well as contextual failure
 * information from the implementation.
 */
class ProcessingException extends \RuntimeException {

}
