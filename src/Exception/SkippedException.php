<?php

namespace Drupal\marketo_ma\Exception;

/**
 * Exception thrown for skipped save style failures.
 *
 * Marketo's Lead api will commonly "fail" without failing and return the record
 * skipped with a reason. This allows us to wrap that into something usable.
 */
class SkippedException extends ProcessingException {

  /**
   * List of skip reasons.
   *
   * @var \Drupal\marketo_ma\Exception\SkippedReason[]
   */
  protected $reasons;

  /**
   * Construct a SkipException.
   *
   * @param \Drupal\marketo_ma\Exception\SkippedReason[] $reasons
   *   List of skip reasons.
   * @param string $message
   *   The exception message.
   * @param int $code
   *   The exception code.
   * @param \Throwable|null $previous
   *   The previous throwable used for the exception chaining.
   */
  public function __construct(array $reasons, $message = "", $code = 0, \Throwable $previous = NULL) {
    parent::__construct(
      '',
      $code,
      $previous
    );
    $this->reasons = $reasons;
    $this->setMessage($message);
  }

  /**
   * Add a descriptive message for this failure.
   *
   * @param string $message
   *   The exception message.
   */
  public function setMessage(string $message): void {
    if ($message) {
      $message .= PHP_EOL;
    }
    $message .= 'Something went wrong with your request: ' . json_encode($this->reasons);
    $this->message = $message;
  }

  /**
   * Check if the skipped reasons contain an error code.
   *
   * @param string|int $code
   *   The exception code.
   *
   * @return \Drupal\marketo_ma\Exception\SkippedReason|false
   *   The first matched status detail or false.
   */
  public function containsCode($code) {
    if ($this->reasons) {
      foreach ($this->reasons as $reason) {
        if ($reason->getCode() == $code) {
          return $reason;
        }
      }
    }
    return FALSE;
  }

}
