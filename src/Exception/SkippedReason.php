<?php

namespace Drupal\marketo_ma\Exception;

/**
 * Skip reason.
 */
class SkippedReason implements \JsonSerializable {

  /**
   * Skip reason code.
   *
   * @var string
   */
  protected $code;

  /**
   * Skip reason message.
   *
   * @var string
   */
  protected $reason;

  /**
   * Construct a skip reason.
   *
   * @param string $code
   *   Skip reason code.
   * @param string $reason
   *   Skip reason.
   */
  public function __construct(string $code, string $reason) {
    $this->code = $code;
    $this->reason = $reason;
  }

  /**
   * Get skip reason code.
   *
   * @return string
   *   Skip reason code.
   */
  public function getCode() {
    return $this->code;
  }

  /**
   * Get human-readable skip reason.
   *
   * @return string
   *   Skip reason.
   */
  public function getReason() {
    return $this->reason;
  }

  /**
   * {@inheritDoc}
   */
  #[\ReturnTypeWillChange]
  public function jsonSerialize() {
    return (object) [
      'code' => $this->getCode(),
      'reason' => $this->getReason(),
    ];
  }

}
