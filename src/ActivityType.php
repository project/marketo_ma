<?php

namespace Drupal\marketo_ma;

/**
 * A data object defining a Marketo Activity.
 */
class ActivityType {


  /**
   * A "definition".
   *
   * @var array
   */
  protected $definition;

  /**
   * ActivityType constructor.
   *
   * @param array $definition
   *   The activity type information.
   */
  public function __construct(array $definition = []) {
    $this->definition = $definition;
  }

  /**
   * Get the Marketo activity ID.
   *
   * @return string
   *   The Activity ID.
   */
  public function id() {
    return $this->definition['id'];
  }

  /**
   * Gets the name for a Marketo MA activity type.
   *
   * @return string
   *   The activity type name.
   */
  public function getName() {
    return $this->definition['name'];
  }

  /**
   * Gets the description for a Marketo MA activity type.
   *
   * @return string
   *   The activity type description.
   */
  public function getDescription() {
    return $this->definition['description'] ?? NULL;
  }

  /**
   * Gets the name of the primary attribute for the activity type.
   *
   * @return string
   *   The name of the primary attributes.
   */
  public function getPrimaryAttributeName() {
    return $this->definition['primaryAttribute']['name'] ?? NULL;
  }

}
