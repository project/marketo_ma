<?php

namespace Drupal\marketo_ma\Secrets;

use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * A simple plugin-ish factory.
 *
 * It may make sense to convert this to plugins. Since this will be needed for
 * generating tracking code it could have critical page loading impact, so it
 * currently has very basic logic.
 */
class SecretsFactory {

  /**
   * The Drupal module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Construct a SecretsFactory object.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The Drupal module handler.
   */
  public function __construct(ModuleHandlerInterface $moduleHandler) {
    $this->moduleHandler = $moduleHandler;
  }

  /**
   * Create a secrets data object.
   *
   * @return \Drupal\marketo_ma\Secrets\SecretsInterface
   *   A secrets object for retrieving marketo secrets.
   */
  public function createSecrets() {
    $definition = $this->getDefinition();

    // If the plugin provides a factory method, pass the container to it.
    if (is_subclass_of($definition, 'Drupal\Core\DependencyInjection\ContainerInjectionInterface')) {
      /** @phpstan-ignore-next-line */
      return $definition::create(\Drupal::getContainer());
    }

    // Otherwise, create the plugin directly.
    return new $definition();
  }

  /**
   * Determine which secrets class to use.
   *
   * @return string
   *   The appropriate secrets class. Probably.
   */
  private function getDefinition() {
    // Move choice to settings or config?
    if ($this->moduleHandler->moduleExists('encryption')) {
      return EncryptionSecrets::class;
    }
    elseif ($this->moduleHandler->moduleExists('key')) {
      // @todo allow more native key integration?
      return ImmutableConfigSecrets::class;
    }

    // Throw exception? Emit warning? Extension point?
    return ImmutableConfigSecrets::class;
  }

}
