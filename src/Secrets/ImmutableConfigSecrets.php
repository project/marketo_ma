<?php

namespace Drupal\marketo_ma\Secrets;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\marketo_ma\Service\MarketoMaServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Config secrets that are stored in Immutable config.
 *
 * Note: This allows reading secrets _directly_ out of config. This is handy
 * but if you ever actually this you're doing something wrong. More likely you
 * should be using Key or Encryption to provide some encryption or safe
 * injection into your application.
 */
class ImmutableConfigSecrets implements SecretsInterface, ContainerInjectionInterface {

  /**
   * The Marketo settings config containing our secrets.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * Construct an ImmutableConfigSecrets object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->config = $config_factory->get(MarketoMaServiceInterface::MARKETO_MA_CONFIG_NAME);
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('config.factory'));
  }

  /**
   * {@inheritDoc}
   */
  public function getClientId() {
    return $this->config->get('rest.client_id');
  }

  /**
   * {@inheritDoc}
   */
  public function getClientSecret() {
    return $this->config->get('rest.client_secret');
  }

  /**
   * {@inheritDoc}
   */
  public function getMunchkinApiKey() {
    return $this->config->get('munchkin.api_private_key');
  }

}
