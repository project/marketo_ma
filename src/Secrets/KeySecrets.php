<?php

namespace Drupal\marketo_ma\Secrets;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\key\KeyRepositoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * This would be a native key integration but how do we configure the keys?
 */
class KeySecrets implements SecretsInterface, ContainerInjectionInterface {

  /**
   * The key repository for retrieving our secrets.
   *
   * @var \Drupal\key\KeyRepositoryInterface
   */
  protected $keyRepository;

  /**
   * Construct a KeySecrets object.
   *
   * @param \Drupal\key\KeyRepositoryInterface $key_repository
   *   The key repository for retrieving our secrets.
   */
  public function __construct(KeyRepositoryInterface $key_repository) {
    $this->keyRepository = $key_repository;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('key.repository'));
  }

  /**
   * {@inheritDoc}
   */
  public function getClientId() {
    return $this->keyRepository->getKey('marketo_api_private_key')->getKeyValue();
  }

  /**
   * {@inheritDoc}
   */
  public function getClientSecret() {
    return $this->keyRepository->getKey('marketo_rest_client_secret')->getKeyValue();
  }

  /**
   * {@inheritDoc}
   */
  public function getMunchkinApiKey() {
    return $this->keyRepository->getKey('marketo_api_private_key')->getKeyValue();
  }

}
