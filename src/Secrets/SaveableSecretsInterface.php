<?php

namespace Drupal\marketo_ma\Secrets;

/**
 * Interface for secret stores that can save the secret.
 */
interface SaveableSecretsInterface {

  /**
   * Save secrets.
   *
   * @param string $munchkin_private_key
   *   Munchkin private key used for signing.
   * @param string $rest_id
   *   Client ID used for REST API.
   * @param string $rest_secret
   *   Client secret used for REST API.
   */
  public function save(string $munchkin_private_key, string $rest_id, string $rest_secret);

}
