<?php

namespace Drupal\marketo_ma\Secrets;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\Site\Settings;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\encryption\EncryptionTrait;
use Drupal\marketo_ma\Service\MarketoMaServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Secrets object using encryption module.
 */
class EncryptionSecrets implements SecretsInterface, SaveableSecretsInterface, ContainerInjectionInterface {

  // Adds ability to encrypt/decrypt configuration.
  use EncryptionTrait;
  // Helpers for sending out warnings.
  use StringTranslationTrait;
  use MessengerTrait;

  /**
   * Configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Marketo settings config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig|null
   */
  protected $config;

  /**
   * Construct an encryption secret object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Configuration factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('config.factory'));
  }

  /**
   * Get's marketo_ma settings.
   *
   * @return \Drupal\Core\Config\Config
   *   Marketo settings config.
   */
  protected function config($edit = FALSE) {
    $encryption_key = Settings::get('encryption_key');
    if (empty($encryption_key)) {
      $this->messenger()->addWarning(
        $this->t(
          'Encryption key is required to be setup in settings.php first. Check Encryption module README file for instructions.'
        )
      );
    }

    if ($edit) {
      return $this->configFactory->getEditable(
        MarketoMaServiceInterface::MARKETO_MA_CONFIG_NAME
      );
    }
    if (!isset($this->config)) {
      $this->config = $this->configFactory->get(
        MarketoMaServiceInterface::MARKETO_MA_CONFIG_NAME
      );
    }
    return $this->config;
  }

  /**
   * {@inheritDoc}
   */
  public function getClientId() {
    return $this->decrypt($this->config()->get('rest.client_id'));
  }

  /**
   * {@inheritDoc}
   */
  public function getClientSecret() {
    return $this->decrypt($this->config()->get('rest.client_secret'));
  }

  /**
   * {@inheritDoc}
   */
  public function getMunchkinApiKey() {
    return $this->decrypt($this->config()->get('munchkin.api_private_key'));
  }

  /**
   * {@inheritDoc}
   */
  public function save(string $munchkin_private_key, string $rest_id, string $rest_secret) {
    $this->config(TRUE)
      ->set('munchkin.api_private_key', $this->encrypt($munchkin_private_key))
      ->set('rest.client_id', $this->encrypt($rest_id))
      ->set('rest.client_secret', $this->encrypt($rest_secret))
      ->save();
  }

}
