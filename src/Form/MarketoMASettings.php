<?php

namespace Drupal\marketo_ma\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\marketo_ma\Secrets\SaveableSecretsInterface;
use Drupal\marketo_ma\Secrets\SecretsInterface;
use Drupal\marketo_ma\Service\MarketoMaServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Settings configuration form.
 *
 * @package Drupal\marketo_ma\Form
 */
class MarketoMASettings extends ConfigFormBase {

  /**
   * The Marketo MA core service.
   *
   * @var \Drupal\marketo_ma\Service\MarketoMaServiceInterface
   */
  protected $service;

  /**
   * Configuration secrets.
   *
   * @var \Drupal\marketo_ma\Secrets\SecretsInterface
   */
  private $secrets;

  /**
   * Constructs a \Drupal\marketo_ma\Form\MarketoMASettings object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\marketo_ma\Secrets\SecretsInterface $secrets
   *   The marketo secrets service.
   * @param \Drupal\marketo_ma\Service\MarketoMaServiceInterface $service
   *   The marketo service.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    SecretsInterface $secrets,
    MarketoMaServiceInterface $service
  ) {
    parent::__construct($config_factory);
    $this->service = $service;
    $this->secrets = $secrets;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('marketo_ma.secrets'),
      $container->get('marketo_ma')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [MarketoMaServiceInterface::MARKETO_MA_CONFIG_NAME];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'marketo_ma_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /** @var array $form */
    $form = parent::buildForm($form, $form_state);

    // Get the configuration.
    $config = $this->config(MarketoMaServiceInterface::MARKETO_MA_CONFIG_NAME);

    $form['marketo_ma_basic'] = [
      '#title' => $this->t('Basic Settings'),
      '#type' => 'fieldset',
    ];
    $form['marketo_ma_basic']['munchkin_account_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Account ID'),
      '#required' => TRUE,
      // @see http://developers.marketo.com/blog/server-side-form-post/
      '#description' => $this->t('In Marketo, go to Admin > Munchkin and copy the Munchkin Account ID, which has the format of 000-AAA-000'),
      '#maxlength' => 128,
      '#size' => 64,
      '#default_value' => $config->get('munchkin.account_id'),
    ];
    $form['marketo_ma_basic']['logging'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Verbose Logging'),
      '#default_value' => $config->get('logging'),
      '#description' => $this->t('If checked, additional data will be added to watchdog.'),
      // @todo this doesn't do anything ATM.
      '#access' => FALSE,
    ];
    $form['marketo_ma_basic']['tracking_method'] = [
      '#type' => 'radios',
      '#title' => $this->t('Tracking Method'),
      '#description' => $this->t(':desc<br /><ol><li>:opt1</li><li>:opt2</li></ol>', [
        ':desc' => 'Select how tracking should be handled.',
        ':opt1' => 'Munchkin API | Client side JS.',
        ':opt2' => 'REST API | Via the REST API on the server side.',
      ]),
      '#options' => [
        MarketoMaServiceInterface::TRACKING_METHOD_MUNCHKIN => $this->t('Munchkin Javascript API'),
        MarketoMaServiceInterface::TRACKING_METHOD_API => $this->t('REST API'),
      ],
      '#default_value' => $config->get('tracking_method'),
      '#required' => TRUE,
    ];

    $form['api'] = [
      '#title' => $this->t('API Configuration'),
      '#type' => 'fieldset',
      '#description' => $this->t('You will need an api user and service configured for this application. See @link for details.', [
        '@link' => Link::fromTextAndUrl(
          $this->t('Quick Start Guide for Marketo REST API'),
          Url::fromUri('https://developers.marketo.com/blog/quick-start-guide-for-marketo-rest-api/')
        )->toString(),
      ]),
    ];
    $form['api']['instance_host'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Marketo Instance Host'),
      '#default_value' => $config->get('instance_host'),
      '#required' => FALSE,
      '#description' => $this->t('Host for your Marketo instance. Example: app-sjqe.marketo.com. Used for Forms 2.0 API.'),
      // @todo this doesn't do anything ATM.
      '#access' => FALSE,
    ];

    $form['api']['rest_client_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client Id'),
      '#default_value' => $this->secrets->getClientId(),
      '#description' => $this->t('Client ID is established as part of a <a href="@url">Custom Service</a>.', ['@url' => 'http://developers.marketo.com/documentation/rest/custom-service/']),
    ];
    $form['api']['rest_client_secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client Secret'),
      '#default_value' => $this->secrets->getClientSecret(),
      '#description' => $this->t('Client Secret is established as part of a <a href="@url">Custom Service</a>.', ['@url' => 'http://developers.marketo.com/documentation/rest/custom-service/']),
    ];
    $form['api']['rest_batch_requests'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Batch API transactions'),
      '#description' => $this->t('Will queue activity and send data to Marketo when cron runs.'),
      '#default_value' => $config->get('rest.batch_requests'),
    ];

    $form['munchkin'] = [
      '#title' => $this->t('Munchkin Configuration'),
      '#type' => 'fieldset',
    ];
    $form['munchkin']['munchkin_javascript_library'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Munchkin Javascript Library'),
      '#default_value' => $config->get('munchkin.javascript_library'),
      '#required' => TRUE,
      '#description' => $this->t('Typically this does not need to be changed and should use the default value //munchkin.marketo.net/munchkin.js'),
    ];
    $form['munchkin']['munchkin_api_private_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Private Key'),
      '#default_value' => $this->secrets->getMunchkinApiKey(),
      '#description' => $this->t('Value can be found on the Munchkin Admin page at Admin > Integration > Munchkin'),
    ];

    $form['munchkin']['marketo_ma_munchkin_advanced'] = [
      '#title' => $this->t('Advanced Initialization Parameters'),
      '#type' => 'details',
      '#description' => $this->t("Munchkin can accept a variety of additional configuration parameters to customize its behavior.<br />NOTE: Leave the field blank to accept it's default value as defined in munchkin.js"),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    ];
    $form['munchkin']['marketo_ma_munchkin_advanced']['munchkin_partition'] = [
      '#type' => 'textfield',
      '#title' => $this->t('wsInfo - Workspace (Partition)'),
      '#default_value' => $config->get('munchkin.partition'),
      '#required' => FALSE,
      '#description' => $this->t('Takes a string to target a workspace.  This workspace ID is obtained by selecting the Workspace in the Admin -> Munchkin menu.'),
    ];
    $form['munchkin']['marketo_ma_munchkin_advanced']['munchkin_altIds'] = [
      '#type' => 'textfield',
      '#title' => $this->t('altIds'),
      '#default_value' => $config->get('munchkin.altIds'),
      '#required' => FALSE,
      '#description' => $this->t('Accepts an array of Munchkin ID strings.  When enabled, this will duplicate all Web Activity to the targeted subscriptions, based on their Munchkin Id.'),
    ];
    $form['munchkin']['marketo_ma_munchkin_advanced']['munchkin_cookieLifeDays'] = [
      '#type' => 'textfield',
      '#title' => $this->t('cookieLifeDays'),
      '#default_value' => $config->get('munchkin.cookieLifeDays'),
      '#required' => FALSE,
      '#description' => $this->t('Sets the expiry date of any newly created munchkin tracking cookies to this many days in the future. Default is two years.'),
    ];
    $form['munchkin']['marketo_ma_munchkin_advanced']['munchkin_clickTime'] = [
      '#type' => 'textfield',
      '#title' => $this->t('clickTime'),
      '#default_value' => $config->get('munchkin.clickTime'),
      '#required' => FALSE,
      '#description' => $this->t('Sets the number of milliseconds to block after a click to allow for click tracking request.  Reducing will reduce accuracy of click-tracking.'),
    ];
    $form['munchkin']['marketo_ma_munchkin_advanced']['munchkin_cookieAnon'] = [
      '#type' => 'textfield',
      '#title' => $this->t('cookieAnon'),
      '#default_value' => $config->get('munchkin.cookieAnon'),
      '#required' => FALSE,
      '#description' => $this->t('Default true. If set to false, will prevent tracking and cookie-ing of new anonymous leads.  Leads are cookied and tracked after filling out a Marketo form, or clicking through from a Marketo Email.'),
    ];
    $form['munchkin']['marketo_ma_munchkin_advanced']['munchkin_domainLevel'] = [
      '#type' => 'textfield',
      '#title' => $this->t('domainLevel'),
      '#default_value' => $config->get('munchkin.domainLevel'),
      '#required' => FALSE,
      '#description' => $this->t('Default 3.  Setting to 2 allows for proper tracking on two-letter top-level domains.'),
    ];
    $form['munchkin']['marketo_ma_munchkin_advanced']['munchkin_disableClickDelay'] = [
      '#type' => 'textfield',
      '#title' => $this->t('disableClickDelay'),
      '#default_value' => $config->get('munchkin.disableClickDelay'),
      '#required' => FALSE,
      '#description' => $this->t('Default false.  If set to true, disables click tracking delay entirely.  Will reduce accuracy of click tracking.'),
    ];
    $form['munchkin']['marketo_ma_munchkin_advanced']['munchkin_asyncOnly'] = [
      '#type' => 'textfield',
      '#title' => $this->t('asyncOnly'),
      '#default_value' => $config->get('munchkin.asyncOnly'),
      '#required' => FALSE,
      '#description' => $this->t('Default false.  If set to true, will send XHRs asynchronously.'),
    ];

    $form['munchkin']['page_tracking_tab'] = [
      '#title' => $this->t('Page tracking'),
      '#type' => 'details',
      '#description' => $this->t('On which pages should Marketo tracking take place.'),
    ];
    $visibility_request_path_pages = $config->get('tracking.request_path.pages');
    $form['munchkin']['page_tracking_tab']['marketo_ma_visibility_pages'] = [
      '#type' => 'radios',
      '#title' => $this->t('Add tracking to specific pages'),
      '#options' => [
        $this->t('Every page except the listed pages'),
        $this->t('The listed pages only'),
      ],
      '#default_value' => $config->get('tracking.request_path.mode'),
    ];
    $form['munchkin']['page_tracking_tab']['marketo_ma_pages'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Pages'),
      '#title_display' => 'invisible',
      '#default_value' => !empty($visibility_request_path_pages) ? $visibility_request_path_pages : '',
      '#description' => $this->t("Specify pages by using their paths. Enter one path per line. The '*' character is a wildcard. Example paths are %blog for the blog page and %blog-wildcard for every personal blog. %front is the front page.", [
        '%blog' => '/blog',
        '%blog-wildcard' => '/blog/*',
        '%front' => '<front>',
      ]),
      '#rows' => 10,
    ];

    $form['munchkin']['role_tracking_tab'] = [
      '#title' => $this->t('Role tracking'),
      '#type' => 'details',
    ];
    $visibility_user_role_roles = $config->get('tracking.user_role.roles');
    $form['munchkin']['role_tracking_tab']['tracking_roles_visibility'] = [
      '#type' => 'radios',
      '#title' => $this->t('Add tracking for specific roles'),
      '#options' => [
        $this->t('Add to the selected roles only'),
        $this->t('Add to every role except the selected ones'),
      ],
      '#default_value' => $config->get('tracking.user_role.mode'),
    ];
    $form['munchkin']['role_tracking_tab']['tracking_roles'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Roles'),
      '#default_value' => !empty($visibility_user_role_roles) ? $visibility_user_role_roles : [],
      '#options' => array_map('\Drupal\Component\Utility\Html::escape', user_role_names()),
      '#description' => $this->t('If none of the roles are selected, all users will be tracked. If a user has any of the roles checked, that user will be tracked (or excluded, depending on the setting above).'),
    ];

    // Legacy...
    $form['marketo_ma_tabs'] = [
      '#type' => 'vertical_tabs',
      '#default_tab' => 'tab_api',
    ];

    // Hide editable secrets and inform the user.
    $secrets_editable = $this->secrets instanceof SaveableSecretsInterface;
    if (!$secrets_editable) {
      // @todo Replace access with some sort of mangling and messaging.
      $msg = $this->t('This settings is not available because its stored in a secret that is not editable.');
      $form['api']['rest_client_id']['#type'] = 'item';
      $form['api']['rest_client_id']['#markup'] = $msg;
      $form['api']['rest_client_secret']['#type'] = 'item';
      $form['api']['rest_client_secret']['#markup'] = $msg;
      $form['munchkin']['munchkin_api_private_key']['#type'] = 'item';
      $form['munchkin']['munchkin_api_private_key']['#markup'] = $msg;
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    if ($this->secrets instanceof SaveableSecretsInterface) {
      $this->secrets->save(
        $form_state->getValue('munchkin_api_private_key'),
        $form_state->getValue('rest_client_id'),
        $form_state->getValue('rest_client_secret')
      );
    }

    $this->config(MarketoMaServiceInterface::MARKETO_MA_CONFIG_NAME)
      ->set('tracking_method', $form_state->getValue('tracking_method'))
      ->set('instance_host', $form_state->getValue('instance_host'))
      ->set('logging', $form_state->getValue('logging'))
      ->set('munchkin.account_id', $form_state->getValue('munchkin_account_id'))
      ->set('munchkin.javascript_library', $form_state->getValue('munchkin_javascript_library'))
      ->set('munchkin.partition', $form_state->getValue('munchkin_partition'))
      ->set('munchkin.altIds', $form_state->getValue('munchkin_altIds'))
      ->set('munchkin.cookieLifeDays', $form_state->getValue('munchkin_cookieLifeDays'))
      ->set('munchkin.clickTime', $form_state->getValue('munchkin_clickTime'))
      ->set('munchkin.cookieAnon', $form_state->getValue('munchkin_cookieAnon'))
      ->set('munchkin.domainLevel', $form_state->getValue('munchkin_domainLevel'))
      ->set('munchkin.disableClickDelay', $form_state->getValue('munchkin_disableClickDelay'))
      ->set('munchkin.asyncOnly', $form_state->getValue('munchkin_asyncOnly'))
      ->set('rest.batch_requests', $form_state->getValue('rest_batch_requests'))
      ->set('tracking.request_path.mode', $form_state->getValue('marketo_ma_visibility_pages'))
      ->set('tracking.request_path.pages', $form_state->getValue('marketo_ma_pages'))
      ->set('tracking.user_role.mode', $form_state->getValue('tracking_roles_visibility'))
      ->set('tracking.user_role.roles', array_filter($form_state->getValue('tracking_roles')))
      ->save();
  }

}
