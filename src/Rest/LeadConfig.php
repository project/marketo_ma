<?php

namespace Drupal\marketo_ma\Rest;

use NecLimDul\MarketoRest\Lead\Configuration;

/**
 * API configuration setup from Drupal configuration.
 */
class LeadConfig extends Configuration {

  use ConfigTrait;

}
