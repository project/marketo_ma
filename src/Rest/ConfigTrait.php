<?php

namespace Drupal\marketo_ma\Rest;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\marketo_ma\Service\MarketoMaServiceInterface;

/**
 * Helper trait for populating rest configuration objects.
 */
trait ConfigTrait {

  /**
   * Creat a configuration object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    parent::__construct();
    $config = $config_factory->get(MarketoMaServiceInterface::MARKETO_MA_CONFIG_NAME);
    $this->setHost($this->getMarketoBaseUrl($config));
  }

  /**
   * Get the base url for talking to Marketo's REST API.
   *
   * @param \Drupal\Core\Config\ImmutableConfig $config
   *   Module configuration data object.
   *
   * @return string
   *   The base url.
   */
  protected function getMarketoBaseUrl(ImmutableConfig $config): string {
    // This matches the legacy url generation but if its future-proof.
    return sprintf('https://%s.mktorest.com', $config->get('munchkin.account_id'));
  }

}
