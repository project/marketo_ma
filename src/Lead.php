<?php

namespace Drupal\marketo_ma;

/**
 * Provides a lead object for the Marketo MA module.
 *
 * @package Drupal\marketo_ma
 */
class Lead {

  /**
   * The lead data.
   *
   * @var array
   */
  protected $data;

  /**
   * The form identifier that should be used during sync.
   *
   * @var int
   */
  protected $formId;

  /**
   * Marketo program name.
   *
   * @var string
   */
  protected $programName;

  /**
   * Marketo tracking cookie.
   *
   * @var string
   */
  protected $cookie;

  /**
   * Constructs a \Drupal\marketo_ma\Lead object.
   *
   * @param array $lead_data
   *   Initial lead data.
   */
  public function __construct(array $lead_data = []) {
    // Set the lead data for this lead.
    $this->data = $lead_data;
  }

  /**
   * Get the Marketo MA lead's email address.
   *
   * @return string
   *   The Lead's email.
   */
  public function getEmail() {
    return $this->data['email'] ?? ($this->data['Email'] ?: NULL);
  }

  /**
   * Get the Marketo MA lead's Marketo ID.
   *
   * @return string
   *   The Lead's marketo ID.
   */
  public function id() {
    return $this->get('id');
  }

  /**
   * Get a specific lead value.
   *
   * @param string $data_key
   *   The key used to store the data. i.e. "email".
   *
   * @return mixed
   *   The requested value.
   */
  public function get($data_key) {
    return !empty($this->data[$data_key]) ? $this->data[$data_key] : NULL;

  }

  /**
   * Set a specific lead value.
   *
   * @param string $data_key
   *   The key used to store the data. i.e. "email".
   * @param mixed $value
   *   The value of the lead field.
   *
   * @return $this
   */
  public function set($data_key, $value) {
    $this->data[$data_key] = $value;
    return $this;
  }

  /**
   * Get addition field data associated with this lead.
   *
   * Field values should be keyed by their field name(rest) to allow mapping.
   *
   * @return array
   *   Additional lead fields.
   */
  public function data() {
    return $this->data;
  }

  /**
   * Get the form id if one is set.
   *
   * @return int|null
   *   The form identifier that should be used during sync if set.
   */
  public function getFormId() {
    return $this->formId;
  }

  /**
   * Get the program name if one is set.
   *
   * @return string|null
   *   The program name that should be used during sync if set.
   */
  public function getProgramName() {
    return $this->programName;
  }

  /**
   * Set a form ID.
   *
   * Used for toggling a form submission instead of a normal lead sync. Since
   * form submissions are a special sort of activity used in marketing and
   * marketo this can be useful for triggering something like a "registration"
   * or other logic instead of a normal syncing.
   *
   * @param int $formId
   *   The form identifier.
   *
   * @return $this
   */
  public function setFormId($formId) {
    $this->formId = $formId;
    return $this;
  }

  /**
   * Set a program name.
   *
   * @param string $programName
   *   The program name.
   *
   * @return $this
   */
  public function setProgramName($programName) {
    $this->programName = $programName;
    return $this;
  }

  /**
   * Get the current session "_mkto_trk" cookie value.
   *
   * @return string
   *   The value of the marketo tracking cookie if available.
   */
  public function getCookie() {
    return $this->cookie;
  }

  /**
   * Sets the leads tracking cookie value.
   *
   * @param string $value
   *   The value from the "_mkto_trk" cookie.
   *
   * @return $this
   */
  public function setCookie($value) {
    $this->cookie = $value;
    return $this;
  }

}
